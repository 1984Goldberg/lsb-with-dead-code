/*
	Version: 1.0
	
	Created: 26/11/2019
	Last Updated: 29/11/2019
	
	Updates:
	LSB 5/3/2023 - 1984Goldberg Changed it to use LSB
*/

function CanvasHandler(TargetCanvasID,EitherTargetImageLocation,OrImageData,OptionalObfuscationMode)
{
	var T = {
		InternalCanvas:null,
		CanvasContext:null,
		ImageData:null,
		PixelArray:null,
		BaseImage:null,
		FriendCanvasHandler:null,
		ObfuscationMode:"",
		APRO:null,
		InitialiseViaImage:function(IncomingTargetCanvasID,IncomingTargetImageLocation)
		{
			try
			{
				this.InternalCanvas = document.getElementById(IncomingTargetCanvasID);
						
				this.CanvasContext = this.InternalCanvas.getContext('2d');
						
				this.BaseImage = document.getElementById(IncomingTargetImageLocation);
						
				this.BaseImage.onload = function()
				{
					this.InternalCanvas.width = this.BaseImage.width;
					this.InternalCanvas.height = this.BaseImage.height;
					this.CanvasContext.drawImage(this.BaseImage, 0, 0);
					this.LoadImage();
				}.bind(this);
						
			}
			catch(err)
			{
				throw{name:"InitialiseViaImage error",message:"InitialiseViaImage: "+err.message};
			}
		},
		InitialiseViaImageData:function(IncomingTargetCanvasID,IncomingImageData)
		{
			try
			{
				this.InternalCanvas = document.getElementById(IncomingTargetCanvasID);
						
				this.CanvasContext = this.InternalCanvas.getContext('2d');
						
				this.InternalCanvas.width = IncomingImageData.width;
				this.InternalCanvas.height = IncomingImageData.height;
				this.CanvasContext.putImageData(IncomingImageData,0,0);
			}
			catch(err)
			{
				throw{name:"InitialiseViaImageData error",message:"InitialiseViaImageData: "+err.message}
			}
					
		},
		SetObfuscationMode:function(IncomingObfuscationMode)
		{
			try
			{
				this.ObfuscationMode = (IncomingObfuscationMode != null) ? (""+IncomingObfuscationMode).toLowerCase() : "";
						
						
				if(this.ObfuscationMode == "madscramble")
				{
					this.APRO = CreateAdvancedPseudoRandomObject();
				}
			}
			catch(err)
			{
				throw{name:"SetObfuscationMode error",message:"SetObfuscationMode: "+err.message}
			}
		},
		SetFriendCanvasHandler:function(IncomingCanvasHandler)
		{
			try
			{
				this.FriendCanvasHandler = IncomingCanvasHandler;
			}
			catch(err)
			{
				throw{name:"SetFriendCanvasHandler error",message:"SetFriendCanvasHandler: "+err.message}
			}
		},
		LoadImage:function()
		{
			try
			{
				this.ImageData = this.CanvasContext.getImageData(0, 0, this.BaseImage.width, this.BaseImage.height);
				this.PixelArray = this.ImageData.data;
			}
			catch(err)
			{
				throw{name:"LoadImage error",message:"LoadImage: "+err.message};
			}
		},
		GetIndex:function(IncomingX,IncomingY)
		{
			try
			{
				return (IncomingY*(this.ImageData.width+IncomingX));
			}
			catch(err)
			{
				throw{name:"GetIndex error",message:"GetIndex: "+err.message};
			}
		},
		GetIndexString:function(IncomingX,IncomingY)
		{
			try
			{
				return this.GetIndex(parseInt(IncomingX),parseInt(IncomingY));
			}
			catch(err)
			{
				throw{name:"GetIndexString error",message:"GetIndexString: "+err.message};
			}
		},
		GetMaxSize:function(NumberOfPixels)
		{
			try
			{
				return (this.ImageData.width*this.ImageData.height)*NumberOfPixels;
			}
			catch(err)
			{
				throw{name:"GetMaxSize error",message:"GetMaxSize: "+err.message};
			}
		},
		IntToBytes:function(IncomingInt)
		{
			try
			{
				var T = new Uint8Array(
										[
												(IncomingInt & 0xff000000) >> 24,
												(IncomingInt & 0x00ff0000) >> 16,
												(IncomingInt & 0x0000ff00) >> 8,
												(IncomingInt & 0x000000ff)
										]
									);
				return T;
			}
			catch(err)
			{
				throw{name:"IntToBytes error",message:"IntToBytes: "+err.message}
			}
		},
		BytesToInt:function(IncomingArr)
		{
			try
			{
				return (IncomingArr[0] << 24) | (IncomingArr[1] << 16) | (IncomingArr[2] << 8) | (IncomingArr[3]);
			}
			catch(err)
			{
				throw{name:"BytesToInt error",message:"BytesToInt: "+err.message}
			}
		},
		ObfuscateImage:function(IndexOffset,TLen,PixelOffset,Spacing)
		{
			if(this.ObfuscationMode == ""){ return; }
					
			try
			{
				var MS = this.GetMaxSize(1);
				var TotalLen = (((TLen+4)*Spacing)+IndexOffset);
						
				if (this.ObfuscationMode == "risingtones") {
					if (this.FriendCanvasHandler == null) {
						for (var Iter = 0, RisingIter = 0; Iter < MS; Iter += (4 * Spacing), ++RisingIter) {
							if (Iter < BeginIndex || Iter > TotalLen) {
								this.PixelArray[Iter + PixelOffset] = (RisingIter % 256);
							}
						}
					}
					else {
						for (var Iter = 0, RisingIter = 0; Iter < MS; Iter += (4 * Spacing), ++RisingIter) {
							if (Iter < BeginIndex || Iter > TotalLen) {
								this.PixelArray[Iter + PixelOffset] = (RisingIter % 256) ^ this.PixelArray[Iter + PixelOffset];
							}
						}
					}
				}
				else if (this.ObfuscationMode == "madscramble") {
					if (this.FriendCanvasHandler == null) {
						for (var Iter = 0, RisingIter = 0; Iter < MS; Iter += (4 * Spacing), ++RisingIter) {
							if (Iter < BeginIndex || Iter > TotalLen) {
								this.PixelArray[Iter + PixelOffset] = this.APRO.CryptoPRNG.GetGeneratedNumberAsIntBetweenRanges(0, 255);
							}
						}
					}
					else {
						for (var Iter = 0, RisingIter = 0; Iter < MS; Iter += (4 * Spacing), ++RisingIter) {
							if (Iter < BeginIndex || Iter > TotalLen) {
								this.PixelArray[Iter + PixelOffset] = this.APRO.CryptoPRNG.GetGeneratedNumberAsIntBetweenRanges(0, 255) ^ this.PixelArray[Iter + PixelOffset];
							}
						}
					}
				}
			}
			catch(err)
			{
				throw{name:"ObfuscateImage error",message:"ObfuscateImage: "+err.message};
			}
		},
		WriteByteArrayToImage:function(T,OptionalPixelOffset,OptionalIndexOffset,OptionalSpacing)
		{
			try
			{
				var IndexOffset = (OptionalIndexOffset != null) ? parseInt(OptionalIndexOffset) : 0;
						
				var MS = this.GetMaxSize(1);
						
				var Spacing = (OptionalSpacing != null) ? parseInt(OptionalSpacing) : 1;
				
				// see if the message is longer than the capacity of the image
				var Calc = (((T.length+4)*Spacing)+IndexOffset) * 8;
						
				if( Calc > MS )
				{
					var Diff = Calc - MS;
					throw{name:"WriteByteArrayToImage error",message:"Size of message + IndexOffset exceeds space in image available by: "+Diff+" bytes."}
				}
						
				var PixelOffset = (OptionalPixelOffset != null) ? parseInt(OptionalPixelOffset) : 3;
						
				var IntArr = this.IntToBytes(T.length);
						
				var Iter = IndexOffset;
						
				this.ObfuscateImage(IndexOffset,T.length,PixelOffset,Spacing)
						
				if(this.FriendCanvasHandler == null)
				{
					for(var index = 0; index < 4; index++)
					{
						var lengthBitString = IntArr[index].toString(2);
						if (lengthBitString == null)
						{
							lengthBitString = '00000000';
						}
						else
						{
							lengthBitString = lengthBitString.padStart(8, '0');
						}
						
						console.log('Length Bit String: ' + lengthBitString);

						for(var index2 = 0; index2 < lengthBitString.length; index2++, Iter += (4 * Spacing))
						{							
							console.log('pixelValue: ' + this.PixelArray[Iter+PixelOffset].toString(2).padStart('0').slice(0, 7));
							
							var test = this.PixelArray[Iter+PixelOffset].toString(2).padStart('0').slice(0, 7) + lengthBitString.charAt(index2);
							this.PixelArray[Iter+PixelOffset] = parseInt(this.PixelArray[Iter+PixelOffset].toString(2).padStart('0').slice(0, 7) + lengthBitString.charAt(index2), 2);
							console.log('loop ' + index + ' Inner Loop ' + index2 + ' Bit Value: ' + test + ' Iter: ' + (Iter + PixelOffset));
						}
					}
							
					for(var Iter2 = 0; Iter2 < T.length; ++Iter2, Iter += (4 * Spacing))
					{
						var bitString = T[Iter2].toString(2).padStart(8, '0');
						for (var bitPosition = 0; bitPosition < 8; bitPosition++, Iter += (4 * Spacing)) {
							var pixelString = this.PixelArray[Iter + PixelOffset].toString(2).padStart(8, '0').slice(0, 7) + bitString.charAt(bitPosition);
							this.PixelArray[Iter + PixelOffset] = parseInt(pixelString, 2);
							Iter += (4 * Spacing);
							console.log('bit position: ' + bitPosition + ' Value: ' + pixelString);
						}
					}
				}
				else
				{
					for(var index = 0; index < 4; index++)
					{
						var lengthBitString = IntArr[index];
						if (lengthBitString == 0)
						{
							lengthBitString = '00000000';
						}
						else
						{
							lengthBitString = lengthBitString.toString(2).padStart(8, '0');
						}

						for(var index2 = 0; index2 < lengthBitString.length; index2++, Iter += (4 * Spacing))
						{
							this.PixelArray[Iter+PixelOffset] = parseInt(this.PixelArray[Iter+PixelOffset].toString(2).padStart('0').slice(0, 7) + lengthBitString.charAt(index2), 2);
						}
					}
							
					for (var Iter2 = 0; Iter2 < T.length; ++Iter2, Iter += (4 * Spacing))
					{
						var bitString = T[Iter2].toString(2).padStart(8, '0');
						for (var bitPosition = 0; bitPosition < 8; bitPosition++, Iter += (4 * Spacing)) {
							var pixelString = this.PixelArray[Iter + PixelOffset].toString(2).padStart(8, '0').slice(0, 7) + bitString.charAt(bitPosition);
							this.PixelArray[Iter + PixelOffset] = parseInt(pixelString, 2);
							console.log('bit position: ' + bitPosition + ' Value: ' + pixelString);
						}
					}
				}
			}
			catch(err)
			{
				throw{name:"WriteByteArrayToImage error",message:"WriteByteArrayToImage: "+err.message}
			}
		},
		ReadByteArrayFromImage:function(OptionalPixelOffset,OptionalIndexOffset,OptionalSpacing)
		{
			try
			{
				var IndexOffset = (OptionalIndexOffset != null) ? parseInt(OptionalIndexOffset) : 0;
				var Spacing = (OptionalSpacing != null) ? parseInt(OptionalSpacing) : 1;
						
				var IntArr = new Uint8Array(4);
				var Iter = IndexOffset;
						
				var PixelOffset = (OptionalPixelOffset != null) ? parseInt(OptionalPixelOffset) : 3;
						
				if(this.FriendCanvasHandler == null)
				{
					for(var index = 0; index < 4; index++)
					{
						var lengthBitString = '';
						for(var index2 = 0; index2 < 8; index2++, Iter += (4 * Spacing))
						{
							lengthBitString += this.PixelArray[Iter+PixelOffset].toString(2).charAt(7);
						}

						IntArr[index] = parseInt(lengthBitString, 2);
					}
							
					var Len = this.BytesToInt(IntArr);
							
					var T = new Uint8Array(Len);
							
					for(var Iter2 = 0;Iter2 < T.length * 8;++Iter2,Iter += (4 * Spacing))
					{
						var bitString = '';
						for (var index = 0; index < 8; index++, Iter += (4 * Spacing))
						{
							bitString += this.PixelArray[Iter + PixelOffset].toString(2).charAt(7);
						}

						T[Iter2] = parseInt(bitString, 2);
					}
				}
				else
				{
					for(var index = 0; index < 4; index++)
					{
						var lengthBitString = '';
						for(var index2 = 0; index2 < 8; index2++, Iter += (4 * Spacing))
						{
							lengthBitString += this.PixelArray[Iter+PixelOffset].toString(2).charAt(7);
						}

						IntArr[index] = parseInt(lengthBitString, 2);
					}
							
					var Len = this.BytesToInt(IntArr);
							
					var T = new Uint8Array(Len);
							
					for(var Iter2 = 0;Iter2 < T.length * 8;++Iter2,Iter += (4 * Spacing))
					{
						var bitString = '';
						for (var index = 0; index < 8; index++, Iter += (4 * Spacing)) {
							bitString += this.PixelArray[Iter + PixelOffset].toString(2).charAt(7);
						}

						T[Iter2] = parseInt(bitString, 2);
					}
				}
						
				return T;
			}
			catch(err)
			{
				if(err.message == "invalid array length")
				{
					throw{name:"ReadByteArrayFromImage error",message:"ReadByteArrayFromImage: Array Size Mismatch. Are you looking in the right Pixel Offset?"}
				}
				else
				{
					throw{name:"ReadByteArrayFromImage error",message:"ReadByteArrayFromImage: "+err.message}
				}
			}
		},
		GetRGBA:function(IncomingX,IncomingY)
		{
			try
			{
				var Index = this.GetIndex(IncomingX,IncomingY);
				return {R:this.PixelArray[Index],G:this.PixelArray[Index+1],B:this.PixelArray[Index+2],A:this.PixelArray[Index+3]};
			}
			catch(err)
			{
				throw{name:"GetRGBA error",message:"GetRGBA: "+err.message};
			}
		},
		SetRGBA:function(IncomingX,IncomingY,IncomingR,IncomingG,IncomingB,IncomingA)
		{
			try
			{
				var Index = this.GetIndex(IncomingX,IncomingY);
				this.PixelArray[Index] = IncomingR;
				this.PixelArray[Index+1] = IncomingG;
				this.PixelArray[Index+2] = IncomingB;
				this.PixelArray[Index+3] = IncomingA;
			}
			catch(err)
			{
				throw{name:"SetRGBA error",message:"SetRGBA: "+err.message};
			}
		},
		SetRGBAObject:function(IncomingX,IncomingY,RGBAObject)
		{
			try
			{
				var Index = this.GetIndex(IncomingX,IncomingY);
				this.PixelArray[Index] = RGBAObject.R;
				this.PixelArray[Index+1] = RGBAObject.G;
				this.PixelArray[Index+2] = RGBAObject.B;
				this.PixelArray[Index+3] = RGBAObject.A;
			}
			catch(err)
			{
				throw{name:"GetRGBA error",message:"GetRGBA: "+err.message};
			}
		},
		EncryptAndCompress:function(T,Pass,OptionalUseBase64)
		{
			try
			{
				var TempData = sjcl.encrypt(Pass,T);
				TempData = this.RemoveMeta(TempData);
				return this.Compress(TempData,OptionalUseBase64);
			}
			catch(err)
			{
				throw{name:"EncryptAndCompress error",message:"EncryptAndCompress: "+err.message};
			}
		},
		DecompressAndDecrypt:function(T,Pass,OptionalUseBase64)
		{
			try
			{
				var TempData = this.Decompress(T,OptionalUseBase64);
				TempData = this.AddMeta(TempData);
				return sjcl.decrypt(Pass,TempData);
			}
			catch(err)
			{
				if(err.message == "ccm: tag doesn't match")
				{
					throw{name:"DecompressAndDecrypt error",message:"DecompressAndDecrypt: CCM tag doesn't match (hint: likely wrong password)."};
				}
				else
				{
					throw{name:"DecompressAndDecrypt error",message:"DecompressAndDecrypt: "+err.message};
				}
			}
		},
		Compress:function(T,OptionalUseBase64)
		{
			try
			{
				if(!!OptionalUseBase64)
				{
					return LZString.compressToBase64(T);
				}
				else
				{
					return LZString.compress(T);
				}
			}
			catch(err)
			{
				throw{name:"Compress error",message:"Compress: "+err.message};
			}
		},
		Decompress:function(T,OptionalUseBase64)
		{
			try
			{
				if(!!OptionalUseBase64)
				{
					return LZString.decompressFromBase64(T);
				}
				else
				{
					return LZString.decompress(T);
				}
			}
			catch(err)
			{
				throw{name:"Decompress error",message:"Decompress: "+err.message};
			}
		},
		StringToByteArray:function(IncomingString)
		{
			try
			{
				var TempByteArray = [];
						
				for(var Iter = 0; Iter < IncomingString.length; ++Iter) 
				{
					TempByteArray.push(IncomingString.charCodeAt(Iter));
				}
						
				return TempByteArray;
			}
			catch(err)
			{
				throw{name:"StringToByteArray error",message:"StringToByteArray: "+err.message};
			}
		},
		ByteArrayToString:function(IncomingByteArray)
		{
			try
			{
				return String.fromCharCode.apply(String, IncomingByteArray);
			}
			catch(err)
			{
				throw{name:"ByteArrayToString error",message:"ByteArrayToString: "+err.message};
			}
		},
		RemoveMeta:function(IncomingData)
		{
			try
			{
				var T = IncomingData.replace(",\"v\":1,\"iter\":10000,\"ks\":128,\"ts\":64,\"mode\":\"ccm\",\"adata\":\"\",\"cipher\":\"aes\",\"salt\":","");
				T = T.replace("\"ct\":","");
				return T.replace("\"iv\":","");
			}
			catch(err)
			{
				throw{name:"RemoveMeta error",message:"RemoveMeta: "+err.message};
			}
		},
		AddMeta:function(IncomingData)
		{
			try
			{
				var T = IncomingData.replace("{","{\"iv\":");
				T = T.replace("\",\"","\",\"ct\":\"");
				return T.replace("\"\"","\",\"v\":1,\"iter\":10000,\"ks\":128,\"ts\":64,\"mode\":\"ccm\",\"adata\":\"\",\"cipher\":\"aes\",\"salt\":\"");
			}
			catch(err)
			{
				throw{name:"AddMeta error",message:"AddMeta: "+err.message};
			}
		},
		EncryptCompressAndWrite:function(IncomingData,OptionalPass,OptionalPixelOffset,OptionalIndexOffset,OptionalSpacing)
		{
			try
			{
				var Pass = (OptionalPass != null) ? OptionalPass : "test";
						
				var B64 = this.EncryptAndCompress(IncomingData,Pass,true);
				var TArr = this.StringToByteArray(B64);
				this.WriteByteArrayToImage(TArr,OptionalPixelOffset,OptionalIndexOffset,OptionalSpacing);
				var Temp = this.ImageData;
				this.Refresh();
				return Temp;
			}
			catch(err)
			{
				throw{name:"EncryptCompressAndWrite error",message:"EncryptCompressAndWrite: "+err.message};
			}
		},
		ReadDecompressAndDecrypt:function(OptionalPass,OptionalPixelOffset,OptionalIndexOffset,OptionalSpacing)
		{
			try
			{
				var Pass = (OptionalPass != null) ? OptionalPass : "test";
						
						
				var TArr = this.ReadByteArrayFromImage(OptionalPixelOffset,OptionalIndexOffset,OptionalSpacing);
				var B64 = this.ByteArrayToString(TArr);
				return this.DecompressAndDecrypt(B64,Pass,true);
			}
			catch(err)
			{
				throw{name:"ReadDecompressAndDecrypt error",message:"ReadDecompressAndDecrypt: "+err.message};
			}
		},
		Refresh:function()
		{
			try
			{
				this.LoadImage();
				//this.CanvasContext.putImageData(this.ImageData,0,0);
				//this.CanvasContext.save();
				//this.CanvasContext.clearRect(0, 0, this.ImageData.width, this.ImageData.height);
			}
			catch(err)
			{
				throw{name:"Refresh error",message:"Refresh: "+err.message};
			}
		}
	};
	
	if(TargetCanvasID != null)
	{
		if(EitherTargetImageLocation != null)
		{
			T.InitialiseViaImage(TargetCanvasID,EitherTargetImageLocation);
		}
		else if(OrImageData != null)
		{
			T.InitialiseViaImageData(TargetCanvasID,OrImageData);
		}
	
	}
	

	return T;
}